package store;

public class BikesDiscount implements DiscountCalculator{

	@Override
	public float calculateDiscount(OrderItem item) {
		float discount = 0;
		float itemAmount = item.getProduct().getUnitPrice() * item.getQuantity();
		if (itemAmount >= 100) {
			discount = itemAmount * 20 / 100;
		}
		return discount;
	}
}
